echo go_ley('http://f.pil.tw/post_new.php');

function go_ley($url, $data = '', $ref = '',$mycookie='',$token='',$posty='n'){    
    $rootdir = dirname(__FILE__);
    $SSL = substr($url, 0, 8) == "https://" ? true : false;  
	if(empty($ref)) $ref = $url;	
	
	
    $ch = curl_init();  
    curl_setopt($ch, CURLOPT_URL, $url);  

	curl_setopt($ch, CURLOPT_TIMEOUT, 30);  
	curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);  
	curl_setopt($ch, CURLOPT_REFERER, $ref);
	curl_setopt($ch, CURLOPT_HEADER, 1);
	curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36"); 
	
    if ($SSL) {  
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false); 
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2); 
    }
    if($posty == 'y') {
        curl_setopt($ch, CURLOPT_POST, true);
	}
    if(!empty($data)){	
		curl_setopt($ch, CURLOPT_POST, true);  
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data); 
	}
	if(empty($mycookie)){	
		curl_setopt($ch, CURLOPT_COOKIEJAR, $rootdir.'/avday.txt');   	
		curl_setopt($ch, CURLOPT_COOKIEFILE, $rootdir.'/avday.txt');
        if(!empty($token)) {
            curl_setopt($ch, CURLOPT_HTTPHEADER, array("Accept:application/json, text/javascript, */* q=0.01;","X-Requested-With:XMLHttpRequest","X-XSRF-TOKEN:{$token}"));
        }
	}else{
		curl_setopt($ch, CURLOPT_HTTPHEADER, array($mycookie));	
	}
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
    $ret = curl_exec($ch);  
    $curl_errno = curl_errno($ch);
    curl_close($ch);  
	
    return $ret;     
}